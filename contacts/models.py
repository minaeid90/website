from django.db import models

from datetime import datetime


class Contact(models.Model):
    project_name = models.CharField(max_length=256, blank=True, null=True)
    project_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=256)
    phone = models.CharField(max_length=256)
    email = models.EmailField(max_length=254)
    message = models.TextField(blank=True)
    contact_time = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return "{} | {}".format(self.name, self.email)
