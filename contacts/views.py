from django.shortcuts import render, redirect
from django.contrib import messages
from datetime import datetime
from django.core.mail import send_mail

from .models import Contact
from projects.models import Project


def contact(request):
    if request.method == "POST":
        # Get values
        project_name = request.POST.get('project_name', '')
        project_id = request.POST.get('project_id', '')
        name = request.POST.get('name', '')
        email = request.POST.get('email', '')
        phone = request.POST.get('phone', '')
        message = request.POST.get('message', '')
        Contact.objects.create(project_name=project_name,
                               project_id=project_id,
                               name=name,
                               email=email,
                               phone=phone,
                               message=message,
                               contact_time=datetime.now())

        send_mail("New Inquiry", "Receiving new inquiry for porject {}".format(
            project_name), (email,), ('minaeid90@gmail.com',))
        project_slug = str(Project.objects.filter(name=project_name)[0].slug)
    return redirect('projects:details', slug=project_slug)
