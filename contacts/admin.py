from django.contrib import admin

from .models import Contact


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'project_name', 'phone', 'email')
    list_display_links = ('id', 'name')
    list_filter = ('contact_time', 'project_name')
    # inlines = [
    #     Inline,
    # ]
    # raw_id_fields = ('',)
    readonly_fields = ('project_id','project_name')
    search_fields = ('name', 'project_name', 'phone', 'email', 'message')
    # date_hierarchy = ''
    # ordering = ('',)
