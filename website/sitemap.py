from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from projects.models import Project


class StaticViewSitemap(Sitemap):
    def items(self):
        return ['pages:index', 'pages:about', 'pages:contact', 'projects:list']

    def location(self, item):
        return reverse(item)


class PortofoilioSitemap(Sitemap):
    def items(self):
        return Project.objects.filter(is_published=True)

    def lastmod(self, obj):
            return obj.updated_at
