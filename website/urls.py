from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from .sitemap import StaticViewSitemap, PortofoilioSitemap
from django.contrib.sitemaps.views import sitemap
sitemaps = {
    'static': StaticViewSitemap,
    'portofolio': PortofoilioSitemap
}

urlpatterns = [
    url(r'^', include('pages.urls', namespace='pages')),
    url(r'^portofolio/', include('projects.urls', namespace='projects')),
    url(r'^contacts/', include('contacts.urls', namespace='contacts')),
    url(r'^admin/', admin.site.urls),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
