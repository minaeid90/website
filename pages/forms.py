from django import forms
from django.core import validators


def is_not_empty(value):
    if value:
        raise forms.ValidationError("is not empty")


class ContactForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': 'Name', 'class': 'form-control'}),
        max_length=256,
        error_messages={'required': 'Please, enter your name.'},
        label='')
    phone = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': 'phone', 'class': 'form-control'}),
        max_length=100,
        required=False,
        label='')
    email = forms.EmailField(widget=forms.TextInput(
        attrs={'placeholder': 'Email', 'class': 'form-control'}),
        error_messages={'required': 'Please, enter a valid email address.',
                        'invalid': 'Please enter a valid email address.'},
        label='')
    message = forms.CharField(widget=forms.Textarea(
        attrs={'placeholder': 'Message', 'class': 'form-control'}),
        error_messages={'required': 'Please, enter your message.'},
        label='')

    honeybot = forms.CharField(
        widget=forms.HiddenInput, required=False,
        label="Leave empty", validators=[is_not_empty])

