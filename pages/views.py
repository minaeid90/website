from django.shortcuts import render
from django.core.mail import send_mail, EmailMessage
from .forms import ContactForm


def index(request):
    return render(request, 'pages/index.html')


def about(request):
    return render(request, 'pages/about.html')


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            EmailMessage(
                subject='Contact Us: {}'.format(form.cleaned_data['name']),
                body=str(form.cleaned_data['message']),
                from_email='minaeid90@gmail.com',
                to=['minaeid90@gmail.com'],
                reply_to=[form.cleaned_data['email']],
            ).send()
        #     context['success'] = 'Your message was saved successfully! '
            form = ContactForm()
    else:
        form = ContactForm()
    context = {
        "form": form
    }
    return render(request, 'pages/contact.html', context)
