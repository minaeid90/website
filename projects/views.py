from django.shortcuts import render, get_object_or_404

from .models import Project, Screenshot


def list(request):
    projects = Project.objects.all()
    context = {
        'projects': projects
    }
    return render(request, 'projects/list.html', context)


def details(request, slug):
    # Get images of the product
    project = get_object_or_404(Project, slug=slug)
    screenshots = Screenshot.objects.filter(project_id=project.id)
    context = {
        "project": project,
        "screenshots": screenshots
    }
    return render(request, 'projects/details.html', context)
