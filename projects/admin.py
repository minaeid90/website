from django.contrib import admin

from .models import Category, Project, Screenshot


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):

    list_display = ('id', 'name')
    list_filter = ('name',)
    search_fields = ('name',)


class ScreenshotsInline(admin.StackedInline):
    model = Screenshot


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'category', 'is_published')
    list_display_links = ('id', 'name')
    list_filter = ('name', 'category')
    list_editable = ('is_published',)
    inlines = [ScreenshotsInline]
    search_fields = ('name', 'category')
