from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.list, name="list"),
    url(r'details/(?P<slug>[\w-]+)/$', views.details, name="details"),
]
