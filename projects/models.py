from django.db import models
from django.utils.text import slugify
from django.shortcuts import reverse
from django.contrib.sitemaps import ping_google


class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=256)
    slug = models.SlugField(unique=True, blank=True)
    description = models.TextField(blank=True)
    url = models.URLField(max_length=256, blank=True)
    image = models.ImageField(upload_to='projects/%Y/%m/%d', blank=True)
    is_published = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # Todo: check if the name already exists
        self.slug = slugify(self.name)
        super(Project, self).save(*args, **kwargs)
        try:
            ping_google()
        except Exception:
            pass

    def get_absolute_url(self):
        return reverse('projects:details', kwargs={'slug': self.slug})


class Screenshot(models.Model):

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='projects/%Y/%m/%d')


class Quote(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=254)
    phone = models.CharField(max_length=20, blank=True)
    message = models.TextField()
